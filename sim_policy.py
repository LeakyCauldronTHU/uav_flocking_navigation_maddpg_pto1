from Env_2D import EnvUAV
from ddpg import *

STEPS = 1000
NUM_AGENTS = 7
env = EnvUAV(NUM_AGENTS)
agent = DDPG(env)
#File = open('./Result/path_domain.txt','w+')

state = env.reset()
for step in range(STEPS):
    action = agent.test_action(state)
    print('action',action)
    #File.write(str(action[0][1]))
    #File.write(' ')
    env.render()
    #action=np.array([[0,0.5],[0,0.5],[0,0.5]])
            
    state_next,reward,done,_ = env.forward(action)
    for i in range(NUM_AGENTS):
        agent.perceive(state[i],action[i],reward[i],state_next[i],done[i])
        #print('distance to the closest agent:',np.log(2/(state[i,-1]+1)-1)/(-0.02))
    state = state_next
    print('reward',reward)
    if done.any():
        env.end()
        #File.close()
        break




    
